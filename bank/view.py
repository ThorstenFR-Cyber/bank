from flask import Flask, render_template, redirect, url_for, request, session
from werkzeug.utils import secure_filename
import mariadb
import sys
import os

app = Flask(__name__)
app.secret_key = 'taMereEnSlipe'
UPLOAD_FOLDER = 'bank/static/doc/upload/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

tablesDB = {
    "T_Personne":           "`Nom`, `Prenom`, `UserName`, `PassWord`",
    "T_Ecritures":          "`FK_id_regles`, `Date`, `Libelle`, `Debit`, `Solde`",
    "T_PersonnePermission": "`FK_id_personne`, `FK_id_regles`",
    "T_Regles":             "`adresse`, `NoCompte`",
    "T_Log":                "`FK_userId`, `Action`"
}

# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user="luis",
        password="VUQfeZzD01",
        host="hibana.meireles.home",
        port=3306,
        database="Bank"
    )
    conn.autocommit = False
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor()


@app.route('/')
def index():
    session['login'] = False
    return render_template('index.html')


@app.route('/index.html')
def home():
    return render_template('index.html')


@app.route('/login.html', methods=['GET', 'POST'])
def login():
        if 'add_elem' in request.form:
            username = request.form.get('username')
            password = request.form.get('pass')
            cur.execute('SELECT * FROM T_Personne WHERE username = %s AND password = %s', (username, password,))
            account = cur.fetchone()
            if account:
                session['id'] = account[0]
                session['username'] = account[3]
                session['nom'] = account[1]
                session['name'] = account[2]
                session['login'] = True
                log("user login: "+username+" cette utilisateur à reussi à ce connecter")
                return redirect(url_for('dashboard'))
            else:
                return render_template('login.html')
                log("user login: "+username+" n'as pas reussi à ce connecter sur le site")

        return render_template('login.html')


@app.route('/dashboard.html')
def dashboard():
    if session['login'] == False:
        return render_template('403.html'), 403
    return render_template('dashboard.html')


@app.route('/reports.html')
def reports():
    return render_template('reports.html')


@app.route('/user.html', methods=['GET', 'POST'])
def user():
    rowsUsers = selectSql("T_Personne")
    comptes   = selectSql("T_Regles")
    if 'addUser' in request.form:
        username = request.form.get('username')
        nom = request.form.get('nom')
        prenom = request.form.get('prenom')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        values = '"'+nom+'", "'+prenom+'", "'+username+'", "'+password1+'"'
        if password1 == password2:
            addSql("T_Personne", values)
        else:
            print("Error: Mot de passe pas identique")
    
    if 'delUser' in request.form:
        idUser = request.form.get('idUserDel')
        deletSql("T_Personne", idUser)

    if 'giveAcces' in request.form:
        idUser = request.form.get('giveAccesUser')
        idCompte = request.form.get('idCompte')
        values = idUser+', '+idCompte
        addSql("T_PersonnePermission", values)
    return render_template('user.html', rowsUsers = rowsUsers, comptes = comptes)

app.config["IMAGE_UPLOADS"] = "static/doc/upload"

@app.route('/xml.html' , methods=['GET', 'POST'])
def xml():
    if 'xmlPoste' in request.form:
        print("t'est mort")
        if 'file' not in request.files:
            print('no file')
            return redirect(request.url)
        
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename

        if file.filename == '':
            print('no filename')
            log("Error ajoute de fichier xml")
            return redirect(request.url)
        else:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            print("saved file successfully")
            log("Ajoue d'un fichier xml")
            #send file name as parameter to downlad
            return redirect('xml.html')
        
    return render_template('xml.html')

@app.route('/regles.html', methods=['GET', 'POST'])
def regles():
    comptes   = selectSql("T_Regles")
    
    if 'addRegles' in request.form:
        address  = request.form.get('address')
        noCompte = request.form.get('noCompte')
        values = '"'+address+'", "'+noCompte+'"'
        addSql('T_Regles', values)

    if 'delRegles' in request.form:
        idDel = request.form.get('idUserDel')
        deletSql("T_Regles", idDel)
    return render_template('regles.html', comptes = comptes)

@app.route('/logs.html')
def logs():
    logs = selectSql('T_Log', "ORDER BY `T_Log`.`Date` DESC")
    return render_template('logs.html', logs = logs)

@app.route('/errors/maintainece.html')
def maintenace():
    return render_template('/errors/maintenace.html')

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_not_access(e):
    # note that we set the 404 status explicitly
    return render_template('403.html'), 403

#Function add data in db
def addSql(table, values):
    try:
        print(table, values)
        sql = 'INSERT INTO '+table+'('+tablesDB[table]+') value('+values+')'
        print(sql)
        cur.execute(sql)
        
        try: 
            log("add ligne: "+session['username']+"dans la table "+table+" valeurs "+values)
        except mariadb.Error as e:
            print(f"Error:'{e}'")
            log("Error: "+e)

    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)
    conn.commit()

#Function delete data in db        
def deletSql(Table, repere, option="`id`"):
    try:
       info = selectSqlWhere(Table, id)
       info = ' '.join([str(elem) for elem in info])
    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)

    try:
        sql = 'DELETE FROM '+Table+' WHERE '+Table+'.'+option+' = '+repere
        cur.execute(sql)
        conn.commit()

        try:
            log("delet ligne: dans la table "+Table+" valeur "+info)
        except mariadb.Error as e:
            print(f"Error:'{e}'")
            log("Error: "+e)
    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)
    
#Function select all table
def selectSql(table, option=""):
    sql = 'SELECT * FROM '+table+' '+option
    try:
        cur.execute(sql)
    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)
    result = cur.fetchall()
    return result

#Function selecte Where
def selectSqlWhere(table, id):
    sql = 'SELECT * FROM '+table+' WHERE id = '+id
    try:
        cur.execute(sql)
    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)
    result = cur.fetchall()
    return result

#Function insert table log
def log(action):
    try:
        cur.execute('INSERT INTO T_Log (`FK_userId`, `Action`) value( %s, %s)', (session['id'], action))
        conn.commit()
    except mariadb.Error as e:
        print(f"Error:'{e}'")
        log("Error: "+e)