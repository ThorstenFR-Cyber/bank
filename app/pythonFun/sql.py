import mariadb
from flask import session

tablesDB = {
    "T_Personne": "`Nom`, `Prenom`, `UserName`, `PassWord`",
    "T_Ecritures": "`FK_id_regles`, `Date`, `Libelle`, `Debit`, `Solde`",
    "T_PersonnePermission": "`FK_id_personne`, `FK_id_regles`",
    "T_Regles": "`adresse`, `NoCompte`",
    "T_Log": "`FK_userId`, `Action`"
}


# Function add data in db
def addSql(table, values, conn):
    cur = conn.cursor()
    try:
        print(table, values)
        sql = 'INSERT INTO ' + table + '(' + tablesDB[table] + ') value(' + values + ')'
        print(sql)
        cur.execute(sql)

        try:
            log("add ligne: " + session['username'] + "dans la table " + table + " valeurs " + values)
            return 1
        except mariadb.Error as Error:
            print(f"Error:'{Error}'")
            log("Error: " + Error)

    except mariadb.Error as Error:
        print(f"Error:'{Error}'")
        log("Error: " + Error)
    conn.commit()


# Function delete data in db
def deleteSql(table, repere, conn, option="`id`", ):
    cur = conn.cursor()
    try:
        info = selectSql(table, repere, "WHERE id = ")
        info = ' '.join([str(elem) for elem in info])
    except mariadb.Error as error:
        print(f"Error:'{error}'")
        log("Error: " + error)

    try:
        sql = 'DELETE FROM ' + table + ' WHERE ' + table + '.' + option + ' = ' + repere
        cur.execute(sql)
        conn.commit()
        try:
            log("delete line: dans la table " + table + " value " + info)
        except mariadb.Error as error:
            print(f"Error:'{error}'")
            log("Error: " + error)
    except mariadb.Error as error:
        print(f"Error:'{error}'")
        log("Error: " + error)


# Function select all table
def selectSql(table, conn, option=""):
    sql = 'SELECT * FROM ' + table + ' ' + option
    cur = conn.cursor()
    print("---------------------------------------------------------------")
    print(conn)
    print(cur)
    print("---------------------------------------------------------------")

    try:
        cur.execute(sql)
    except mariadb.Error as Error:
        print(f"Error:'{Error}'")
        log("Error: " + Error)
        return 2, Error
    result = cur.fetchall()
    return result


# Function insert table log
def log(action, conn):
    cur = conn.cursor()
    try:
        cur.execute('INSERT INTO T_Log (`FK_userId`, `Action`) value( %s, %s)', (session['id'], action))
        conn.commit()
    except mariadb.Error as Error:
        print(f"Error:'{Error}'")
